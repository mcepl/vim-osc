Set of vim function helping with working on checkouts from 
`openSUSE Build Service`_

.. _`openSUSE Build Service`:
    https://build.opensuse.org/
