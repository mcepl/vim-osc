" ------------------------------------------------------------------------------
" Exit when your app has already been loaded (or "compatible" mode set)
if exists("g:loaded_OSC") || &cp
  finish
endif
let g:loaded_OSC= 1 " your version number

if !hasmapto('ODiff')
  map <unique> <Leader>od call <SID>ODiff()<CR>
endif

" noremap <silent> <unique> <script> ODiff :call <SID>ODiff()<CR>
command! ODiff :call <SID>ODiff()<CR>

fun! s:ODiff()
    let cur_file=expand("%:p")
    let cur_dir=expand("%:p:h")
    let cur_project=join(readfile(cur_dir . '/.osc/_project', ''), '\n')
    let cur_package=join(readfile(cur_dir . '/.osc/_package', ''), '\n')
    let tmpfile=tempname()
    let cmd = "osc -q co -o " . tmpfile . " " . cur_project . " " . cur_package
    let ret = system(cmd)
    let old_file = printf("%s/%s", tmpfile, expand("%:p:t"))
    exe "diffsplit " . old_file
    let ret = system('rm -rf ' . tmpfile)
endfun
